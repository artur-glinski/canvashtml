var audio = new Audio('./KT.mp3');
audio.play();
window.addEventListener('keyup', animation, false);
//window.addEventListener('keydown', finish(), false);


var canvas = document.createElement('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
document.body.appendChild(canvas);
//var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
//------------------------------------------------------------------------------


function animation() {
	requestAnimationFrame(animation);

	var square = {
	x: canvas.width / 2,
	y: canvas.height / 2,
	a: random(10, 200),
	colorR: random(0, 255),
	colorG: random(0, 255),
	colorB: random(0, 255),
	changeX: getRandomCoords(),
	changeY: getRandomCoords()
	};


	ctx.clearRect(0, 0, canvas.width, canvas.height);
	//canvas.style.backgroundColor = 'rgb(' + random(0, 21) + ', ' + random(0, 44) + ', ' + random(0, 33) + ')';
	

		
		ctx.fillStyle = 'rgb(' + square.colorR + ', ' + square.colorG + ', ' + square.colorB + ')';

		square.x += square.changeX;
		square.y += square.changeY;

		square.changeX += 15;
		square.changeY += 15;

		ctx.fillRect(square.x - square.a / 2, square.y - square.a / 2, square.a, square.a);

}

animation();


function getRandomCoords() {
	var change = random(-999, 999) / 100;

	if (change === 0) {
		var change = random(1, 999) / 100;
	}
	return change;
}

function random(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}